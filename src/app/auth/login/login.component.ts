import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../services/login.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  correo = '';
  contrasenna = '';
  error = '';
  form: FormGroup;
  public loginInvalid: boolean;
  private formSubmitAttempt: boolean;
  private returnUrl: string;

  constructor(private loginService: LoginService, private router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });
  }

  login(): void{
    if (this.loginService.login(this.correo, this.contrasenna)) {
      this.error = '';
      this.loginInvalid = false;
      document.location.href = 'http://127.0.0.1:4201/';
    }
    else {
      this.error = 'Usuario y/o contraseña incorrectos';
      this.loginInvalid = true;
    }
  }
  setEmail(): void {
    this.correo = this.form.get('username').value;
  }
  setPassword(): void {
    this.contrasenna = this.form.get('password').value;
  }
}
