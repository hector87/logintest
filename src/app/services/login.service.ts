import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  login(email: string, contrasenna: string): boolean {
    if (email === 'hector@email.com' && contrasenna === '123') {
      localStorage.setItem('user', email);
      return true;
    }
    return false;
  }

  logout(): void {
    localStorage.removeItem('user');
  }
}
