import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router) { }

  canActivate(): boolean {
    if (localStorage.getItem('user')) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
