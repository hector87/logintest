import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {LoginService} from '../../services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  isLogin = false;

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    if (localStorage.getItem('user')) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }
  public logout(): void {
    this.loginService.logout();
    this.isLogin = false;
  }

}
